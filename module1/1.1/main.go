package main

import (
	"fmt"
)

func main() {
	var stringArr = [5]string{"I", "am", "stupid", "and", "weak"}
	for index, value := range stringArr {
		if value == "stupid" {
			stringArr[index] = "smart"
		} else if value == "weak" {
			stringArr[index] = "strong"
		}
	}
	fmt.Println(stringArr)
}