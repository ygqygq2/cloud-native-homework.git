# 作业要求

* 为 HTTPServer 添加 0-2 秒的随机延时；
* 为 HTTPServer 项目添加延时 Metric；
* 将 HTTPServer 部署至测试集群，并完成 Prometheus 配置；
* 从 Promethus 界面中查询延时指标数据；
* 创建一个 Grafana Dashboard 展现延时分配情况。

# 作业过程

1. 部署 httpserver 相关 yaml
`kubectl apply -f deploy/`
浏览器、curl 访问时日志打印延迟时间输出查看 `kubectl log -f httpserver-XXXXXX-XXXXXX`
![访问时随机延时](https://images.linuxba.com/小书匠/2022-4-22/1650598549171.png)

2. 浏览器访问 httpserver metrics，显示出指标，访问url：https://m8.k8snb.com/metrics
![metrics结果](https://images.linuxba.com/小书匠/2022-4-22/1650598589803.png)

3. 部署 lock stack，包含 prometheus、granfa 等
```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
kubectl create ns monitoring  # 创建监控命名空间
helm install loki-stack -n monitoring --set grafana.enabled=true --set prometheus.enabled=true grafana/loki-stack
```
![loki prometheus部署](https://images.linuxba.com/小书匠/2022-4-22/1650598629732.png)

4. 访问 prometheus 
使用 `kubectl port-forward -n monitoring svc/loki-stack-prometheus-server 8000:80` 进行本地端口转发，然后浏览器访问 http://127.0.0.1:8000
![prometheus查询](https://images.linuxba.com/小书匠/2022-4-22/1650598452327.png)

5. 访问 grafana
* 获取 grafana admin 用户密码：`kubectl get secret -n monitoring loki-stack-grafana -ojsonpath={.data.admin-password}|base64 -d`

使用 `kubectl port-forward -n monitoring svc/loki-stack-grafana 8000:80` 进行本地端口转发，然后浏览器访问 http://127.0.0.1:8000
使用 admin 用户名及上面获取到的密码登录 grafana
grafana 导入当前仓库目录 grafana 目录下 json 模板文件
然后多访问下 httpserver。

![grafana监控图表](https://images.linuxba.com/小书匠/2022-4-22/1650598689234.png)
