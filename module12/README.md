# 作业要求

把我们的 httpserver 服务以 Istio Ingress Gateway 的形式发布出来。以下是你需要考虑的几点：

* 如何实现安全保证；
* 七层路由规则；
* 考虑 open tracing 的接入。

# 作业过程

## 安装 istio

```bash
cd  /tmp
wget https://github.com/istio/istio/releases/download/1.13.3/istio-1.13.3-linux-amd64.tar.gz
tar -zxvf istio-1.13.3-linux-amd64.tar.gz
mv istio-1.13.3 /usr/local/
cd /usr/local/
ln -s istio-1.13.3 istio
ln -sf /usr/local/istio/bin/istioctl /usr/local/sbin/
istioctl install --set profile=demo -y
kubectl get pod -n istio-system
kubectl get svc -n kube-system
```

![安装完成istio](https://images.linuxba.com/小书匠/2022-5-6/1651823650956.png)

## 启动应用服务

为 default 命名空间打上标签 istio-injection=enabled：

```bash
kubectl label namespace default istio-injection=enabled
```

使用 `kubectl` 部署应用：
```bash
kubectl apply -f deploy/
```

确认所有服务和 Pod 都已经正确定义和启动，并获取 istio-ingressgateway 的service IP：
```bash
kubectl get pod|grep httpserver
kubectl get svc,ep|grep httpserver
kubectl get svc -n istio-system
```
![应用启动成功](https://images.linuxba.com/小书匠/2022-5-6/1651832413562.png)

* 在 k8s 节点上访问可使用： `istio-ingressgateway clusterIP: 10.100.160.227`
* 在 k8s 节点外部访问可使用： `istio-ingressgateway externalIP: 10.111.3.91`

## 访问测试

在访问的机器上添加 hosts（k8s 节点上访问，可使用 ClusterIP，我的集群使用了 metallb 暴露了 LoadBalancerIP）后，可使用 curl 访问：
```bash
curl https://httpserver.k8snb.com?user=ygqygq2
curl -H "user:ygqygq2" https://httpserver.k8snb.com?user=ygqygq2
```
![curl访问](https://images.linuxba.com/小书匠/2022-5-6/1651832760451.png)

k8s 外部访问也可：
![k8s外部使用curl访问](https://images.linuxba.com/小书匠/2022-5-6/1651832074064.png)

![grafana展示istio监控](https://images.linuxba.com/小书匠/2022-5-6/1651847274411.png)

## 安装 Jaeger

```bash
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.13/samples/addons/jaeger.yaml
```

## tracing 测试

部署 service0、service1及相关 istio 资源：

```bash
kubectl apply -f service0/specs/deployment.yaml
kubectl apply -f service1/specs/deployment.yaml
kubectl apply -f tracing/istio-specs.yaml
```

查看运行情况：
```bash
kubectl get deploy,pod,svc,virtualservice|egrep 'service[0|1]'
```
![查看运行情况](https://images.linuxba.com/小书匠/2022-5-7/1651906463085.png)

通过浏览器访问 service0，我这里使用了 istio ingress gateway 的 external IP 来访问：
http://10.111.3.91/service0
![浏览器访问service0](https://images.linuxba.com/小书匠/2022-5-7/1651906880258.png)

临时端口转发 jaeger service 暴露访问：
```bash
kubectl port-forward svc/tracing -n istio-system 8000:80 --address=0.0.0.0
```

我这里是在 10.111.3.53 上面进行的端口转发，浏览器访问它即可：
http://10.111.3.53:8000/

![浏览器访问jaeger的service0](https://images.linuxba.com/小书匠/2022-5-7/1651907426646.png)

![点击查看详情](https://images.linuxba.com/小书匠/2022-5-7/1651907457508.png)

![调用链详情](https://images.linuxba.com/小书匠/2022-5-7/1651907474473.png)